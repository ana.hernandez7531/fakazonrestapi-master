package com.Fakazon.FakazonSpringBootReact;



import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "cell")
public class Cell {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "nom")
    private String nom;
    
    @Column(name = "categorie")
    private String categorie;
    
    @Column(name = "quantite")
    private int quantite;
    
    @Column(name = "prix")
    private double prix;
    
    public Cell() {}

	public Cell(String nom, String categorie, int quatite, double prix) {
		super();
		this.id = id;
		this.nom = nom;
		this.categorie = categorie;
		this.quantite = quatite;
		this.prix = prix;
	}
	
	public Cell(int id, String nom, String categorie, int quatite, double prix) {
		super();
		this.id = id;
		this.nom = nom;
		this.categorie = categorie;
		this.quantite = quatite;
		this.prix = prix;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quatite) {
		this.quantite = quatite;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	@Override
	public String toString() {
		return "Cell [id=" + id + ", nom=" + nom + ", categorie=" + categorie + ", quatite=" + quantite + ", prix="
				+ prix + "]";
	}

}
