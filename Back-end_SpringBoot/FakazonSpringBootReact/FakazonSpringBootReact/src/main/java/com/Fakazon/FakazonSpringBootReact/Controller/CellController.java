package com.Fakazon.FakazonSpringBootReact.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.Fakazon.FakazonSpringBootReact.Cell;
import com.Fakazon.FakazonSpringBootReact.Repo.FakazonRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
public class CellController {

	@Autowired
	FakazonRepository fakazonRepository;

	// GetAll
	@GetMapping("/cell")
	public List<Cell> index() {
		return fakazonRepository.findAll();
	}

	// Get by Id
	@GetMapping("/cell/{id}")
	public Optional<Cell> show(@PathVariable int id) {

		return fakazonRepository.findById(id);
	}

	// Create a new Cell
	@PostMapping("/cell")
	public Cell create(@RequestBody Map<String, String> body) {
		String nom = body.get("nom");
		String categorie = body.get("categorie");
		int quantite = Integer.parseInt(body.get("quantite"));
		double prix = Double.parseDouble(body.get("prix"));
		return fakazonRepository.save(new Cell(nom, categorie, quantite, prix));
	}

	// Update an existing cell
	@PutMapping("/cell/{id}")
	public Cell update(@PathVariable String id, @RequestBody Map<String, String> body) {
		int idCell = Integer.parseInt(id);

		Cell cell = fakazonRepository.getOne(idCell);
		cell.setNom(body.get("nom"));
		cell.setCategorie(body.get("categorie"));
		cell.setQuantite(Integer.parseInt(body.get("quantite")));
		cell.setPrix(Double.parseDouble(body.get("prix")));

		return fakazonRepository.save(cell);

	}

	// Delete by Id
	@DeleteMapping("cell/{id}")
	public boolean delete(@PathVariable String id) {
		int idCell = Integer.parseInt(id);
		fakazonRepository.deleteById(idCell);
		return true;

	}

}
