package com.Fakazon.FakazonSpringBootReact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FakazonSpringBootReactApplication {

	public static void main(String[] args) {
		SpringApplication.run(FakazonSpringBootReactApplication.class, args);
	}

}
