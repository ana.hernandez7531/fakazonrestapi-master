import React, {useState} from 'react'
import { useNavigate } from 'react-router';
import InputText from './InputText';
import InputNumber from './InputNumber';
import InputSelect from './InputSelect';
import { useSearchParams } from 'react-router-dom';
import CellulaireServices from '../services/CellulaireServices';



const ModifierCellulaire = () => {

  const [query] = useSearchParams()
  const [id, setId] = useState(query.get("id"))
  const [nom, setNom] = useState(query.get("nom"));
  const [prix, setPrix] = useState(query.get("prix"));
  const [categorie, setCategorie] = useState(query.get("categorie"));
  const [quantite, setQuantite] = useState(query.get("quantite"));

  // Navigation apres le POST a l'api
  const navigate = useNavigate();

  const listeCategorie = ["Samsung", "Apple"]
  console.log(categorie)


  // Fonction qui envoie les informations au API
  const updateData = async(e) => {
    e.preventDefault()

    const produit = {nom, prix, categorie, quantite}
    const reponse = await CellulaireServices.modifierCellulaire(produit, id)

    if(reponse.status === 200)
      navigate("/")
    
  }

  return (
    <div>
    <h1 id="titre">Modifier Cellulaire</h1>
    <a href="/">Retour à l'acceuil</a>
    <form>
      <InputText label="Nom" setValue={setNom} value={nom} />
      <InputNumber label="Prix" setValue={setPrix} value={prix} />
      <InputNumber label="Quantité" setValue={setQuantite} value={quantite} />
      <InputSelect label="Catégorie" options={listeCategorie} setValue={setCategorie} value={categorie} />
      <button className='btn btn-info' onClick={updateData} >Modifier</button>
    </form>
  </div>
  )
}
export default ModifierCellulaire