import React, {useState} from 'react'
import { useNavigate} from 'react-router';
import InputText from './InputText';
import InputNumber from './InputNumber';
import InputSelect from './InputSelect';
import CellulaireServices from '../services/CellulaireServices';

const AjouterCellulaire = () => {

  const [nom, setNom] = useState('');
  const [prix, setPrix] = useState('');
  const [categorie, setCategorie] = useState('');
  const [quantite, setQuantite] = useState('');

  // Navigation apres le POST a l'api
  const navigate = useNavigate();

  const listeCategorie = ["Samsung", "Apple"]


  // Fonction qui envoie les informations au API
  const postData = async (e)=>{
    e.preventDefault()
    const response = await CellulaireServices.ajouterCellulaire({nom, prix, categorie, quantite})

    if(response.status === 200)
      navigate("/")

  }

  return (
    <div>
      <h1 id="titre">Ajouter Cellulaire</h1>
      <a href="/">Retour à l'acceuil</a>
      <form>
        <InputText label="Nom" setValue={setNom} value={nom} />
        <InputNumber label="Prix" setValue={setPrix} value={prix} />
        <InputNumber label="Quantité" setValue={setQuantite} value={quantite} />
        <InputSelect label="Catégorie" options={listeCategorie} setValue={setCategorie} value={categorie} />
        <button className='btn btn-info' onClick={postData} >Ajouter</button>
      </form>
    </div>
    
  )
}

export default AjouterCellulaire

